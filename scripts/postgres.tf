

resource "kubernetes_deployment" "postgres" {
  metadata {
    name = "postgres"

    labels = {
      pod = "db"
    }
  }

  spec {
    replicas = 1

    selector {
      match_labels = {
        pod = "db"
      }
    }

    template {
      metadata {
        labels = {
          pod = "db"
        }
      }

      spec {
        container {
          image = "postgres"
          name  = "db"

          port {
            container_port = 5432
          }
          
          env {
            name = "POSTGRES_USER"
            value = "postgres"
          }
          env {
              name = "POSTGRES_PASSWORD"
              value = "postgres"
                       }
            
          }
        }
      }
    }
  }



resource "kubernetes_service" "postgres" {
    metadata {
        name = "db"
    }

    spec {
        selector = {
            pod = kubernetes_deployment.postgres.metadata[0].labels.pod
        }

        port {
            port        = 5432
            target_port = 5432
        }
    }
}

