resource "kubernetes_deployment" "redis" {
  metadata {
    name = "redis"

    labels = {
      pod = "redis"
    }
  }

  spec {
    replicas = 1

    selector {
      match_labels = {
        pod = "redis"
      }
    }

    template {
      metadata {
        labels = {
          pod = "redis"
        }
      }

      spec {
        container {
          image = "redis"
          name  = "redis"

          port {
            container_port = 6379
          }
        }
      }
    }
  }
}

resource "kubernetes_service" "redis" {
    metadata {
        name = "redis"
    }

    spec {
        selector = {
            pod = kubernetes_deployment.redis.metadata[0].labels.pod
        }

        port {
            port        = 6379
            target_port = 6379
        }
    }
}

